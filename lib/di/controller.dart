import 'package:get/get.dart';
import 'package:my_demo/screen/home_screen/home_screen_controller.dart';

Future<void> initControllers() async {
  Get.put<HomeScreenController>(
    HomeScreenController(),
    permanent: true,
  );
}
