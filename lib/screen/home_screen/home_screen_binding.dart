import 'package:get/get.dart';
import 'package:my_demo/screen/home_screen/home_screen_controller.dart';

class HomeScreenBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
          () => HomeScreenController(),
    );
  }
}
