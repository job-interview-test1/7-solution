class FibonacciModel {
  final int fibonacciNumber;
  final int type;
  final int index;

  FibonacciModel({
    required this.fibonacciNumber,
    required this.type,
    required this.index
  });
}

 class FiboType {
  static const int close = 0;
  static const int circle = 1;
  static const int square = 2;
 }