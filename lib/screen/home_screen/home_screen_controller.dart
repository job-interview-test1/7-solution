import 'dart:math';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_demo/screen/home_screen/data_model/data_model.dart';

class HomeScreenController extends GetxController {
  // Obx Variables -------------------------------------------------------------

  RxInt highlight = (-1).obs;
  RxInt mainHighlight = (-1).obs;
  RxBool loading = true.obs;
  RxList<FibonacciModel> fibonacciModel = RxList();
  RxList<FibonacciModel> fibonacciClose = RxList();
  RxList<FibonacciModel> fibonacciSquare = RxList();
  RxList<FibonacciModel> fibonacciCircle = RxList();
  // Variables -----------------------------------------------------------------
  late ScrollController scrollController;
  // Functions -----------------------------------------------------------------


  @override
  void onInit() {
    super.onInit();
    scrollController = ScrollController();
    fibonacciModel.value = generateFibonacci(40);
    loading.value = false;
  }

  List<FibonacciModel> generateFibonacci(int count) {
    List<FibonacciModel> fibonacciList = [];
    int a = 0;
    int b = 1;

    fibonacciList.add(FibonacciModel(fibonacciNumber: a, type: Random().nextInt(3), index: 0));
    fibonacciList.add(FibonacciModel(fibonacciNumber: b, type: Random().nextInt(3), index: 1));
    for (int i = 0; i < (count -2) ; i++) {
      int temp = a;
      a = b;
      b = temp + b;
      fibonacciList.add(FibonacciModel(fibonacciNumber: b, type: Random().nextInt(3), index: i + 2));
    }

    return fibonacciList;
  }

  void removeFiboFromMain(FibonacciModel fibo) {
    if (fibo.type == FiboType.close) {
      fibonacciClose.value.add(fibo);
    } else if (fibo.type == FiboType.circle) {
      fibonacciCircle.value.add(fibo);
    } else {
      fibonacciSquare.value.add(fibo);
    }

    List<FibonacciModel> temp = fibonacciModel.value
        .where((element) =>
    element.index != fibo.index)
        .toList();
    fibonacciModel.value = temp;
    highlight.value = fibo.index;
  }

  void addFiboToMain(FibonacciModel fibo) {
    List<FibonacciModel> temp = fibonacciModel.value;
    temp.add(fibo);
    temp.sort((a, b) => a.index.compareTo(b.index));
    fibonacciModel.value = temp;
    loading.value = true;
    if (fibo.type == 0) {
      fibonacciClose.value.remove(fibo);
    } else if (fibo.type == 1) {
      fibonacciCircle.value.remove(fibo);
    } else {
      fibonacciSquare.value.remove(fibo);
    }
    mainHighlight.value = fibo.index;
    loading.value = false;
    Future.delayed(const Duration(milliseconds: 300), () {
      jumpToIndex(fibo);
    });
  }

  jumpToIndex(FibonacciModel fibo) {
    int index = fibonacciModel.value.indexOf(fibo);
    scrollController.animateTo((52.0 * index), duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
  }

}
