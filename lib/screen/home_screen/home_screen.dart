import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_demo/screen/home_screen/components/v_space.dart';
import 'package:my_demo/screen/home_screen/data_model/data_model.dart';
import 'package:my_demo/screen/home_screen/home_screen_controller.dart';

class HomeScreen extends GetView<HomeScreenController> {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("7 Sulution Test"),
        centerTitle: true,
      ),
      body: Container(
        color: Colors.transparent,
        child: Column(
          children: [
            Obx(
              () => controller.loading.value
                  ? const SizedBox(
                      child: Text("Loading...."),
                    )
                  : Expanded(
                      child: ListView.builder(
                      itemCount: controller.fibonacciModel.value.length,
                      controller: controller.scrollController,
                      itemBuilder: (context, index) {
                        FibonacciModel fibo =
                            controller.fibonacciModel.value[index];
                        return Card(
                          key: Key("Main-$index"),
                          color: controller.mainHighlight.value == fibo.index
                              ? Colors.red
                              : null,
                          child: ListTile(
                            title: GestureDetector(
                                onTap: () {
                                  showModalBottomSheet(
                                    context: context,
                                    builder: (context) {
                                      return inModalList(fibo.type);
                                    },
                                  );
                                },
                                child: Text(
                                    'Index: ${fibo.index}: Number: ${fibo.fibonacciNumber}')),
                            trailing: GestureDetector(
                              onTap: () {
                                controller.removeFiboFromMain(fibo);
                                showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return inModalList(fibo.type);
                                  },
                                );
                              },
                              child: fiboType(fibo.type),
                            ),
                          ),
                        );
                      },
                    )),
            ),
          ],
        ),
      ),
    );
  }

  Widget inModalList(int type) {
    List<FibonacciModel> fiboOfType = [];
    if (type == 0) {
      fiboOfType = controller.fibonacciClose.value;
    } else if (type == 1) {
      fiboOfType = controller.fibonacciCircle.value;
    } else {
      fiboOfType = controller.fibonacciSquare.value;
    }
    return Expanded(
      child: ListView.builder(
        itemCount: fiboOfType.length,
        itemBuilder: (context, index) {
          FibonacciModel fibo = fiboOfType[index];
          return Obx(
            () => GestureDetector(
              onTap: () {
                Navigator.pop(context);
                controller.addFiboToMain(fibo);
              },
              child: Card(
                color: controller.highlight.value == fibo.index
                    ? Colors.green
                    : null,
                child: ListTile(
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Number: ${fibo.fibonacciNumber}'),
                      const VSpace(4),
                      Text('Index: ${fibo.index}'),
                    ],
                  ),
                  trailing: fiboType(fibo.type),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget fiboType(int n) {
    switch (n) {
      case 0:
        return const Icon(Icons.close);
      case 1:
        return const Icon(Icons.circle);
      case 2:
        return const Icon(Icons.square_outlined);
      default:
        return const Icon(Icons.square_outlined);
    }
  }
}
